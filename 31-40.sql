-- 31. Для классов кораблей, калибр орудий которых не менее 16 дюймов, укажите класс и страну.

SELECT class, country
FROM classes
WHERE bore >= 16

-- *32. Одной из характеристик корабля является половина куба калибра его главных орудий (mw). 
-- С точностью до 2 десятичных знаков определите среднее значение mw для кораблей каждой страны,
-- у которой есть корабли в базе данных.

SELECT country, ROUND(AVG(bore * bore * bore / 2), 2) AS WEIGHT
FROM (
SELECT country, bore
FROM classes INNER JOIN ships
ON classes.class = ships.class 
) GROUP BY country

-- 33. Укажите корабли, потопленные в сражениях в Северной Атлантике (North Atlantic). Вывод: ship.

SELECT ship FROM outcomes
WHERE battle = 'North Atlantic' AND result = 'sunk'; 

-- 34. По Вашингтонскому международному договору от начала 1922 г. запрещалось строить линейные корабли водоизмещением более 35 тыс.тонн. Укажите корабли, нарушившие этот договор 
-- (учитывать только корабли c известным годом спуска на воду). Вывести названия кораблей.

SELECT DISTINCT name FROM ships 
WHERE launched IS NOT NULL AND launched >= 1922 AND class IN (
  SELECT DISTINCT class FROM classes
  WHERE displacement > 35000 AND type = 'bb'
)
ORDER BY name;

-- *35. В таблице Product найти модели, которые состоят только из цифр или только из латинских букв (A-Z, без учета регистра).
-- Вывод: номер модели, тип модели.

SELECT DISTINCT model, type FROM product
WHERE REGEXP_LIKE(model, '[0-9]*') OR REGEXP_LIKE(model, '[A-Za-z]*');

-- 36. Перечислите названия головных кораблей, имеющихся в базе данных (учесть корабли в Outcomes).

SELECT DISTINCT name FROM (
SELECT name FROM ships
UNION 
SELECT ship AS name FROM outcomes
) WHERE name IN (
SELECT DISTINCT class FROM classes
)