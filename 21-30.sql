-- 21. Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
-- Вывести: maker, максимальная цена.

SELECT maker, MAX(price) AS MAX_PRICE
FROM product INNER JOIN pc
ON product.model = pc.model
WHERE product.type = 'PC'
GROUP BY maker

-- 22. Для каждого значения скорости ПК, превышающего 600 МГц, определите среднюю цену ПК с такой же скоростью. 
-- Вывести: speed, средняя цена.

SELECT speed, AVG(price) AS AVG_PRICE
FROM pc
WHERE speed > 600
GROUP BY speed

-- 23. Найдите производителей, которые производили бы как ПК со скоростью не менее 750 МГц, 
-- так и ПК-блокноты со скоростью не менее 750 МГц.
-- Вывести: Maker

SELECT maker FROM 
(
SELECT maker
FROM product INNER JOIN laptop
ON product.model = laptop.model
WHERE speed >= 750
INTERSECT
SELECT maker
FROM product INNER JOIN pc
ON product.model = pc.model
WHERE speed >= 750
)
GROUP BY maker;

-- 24. Перечислите номера моделей любых типов, имеющих самую высокую цену по всей имеющейся в базе данных продукции.

SELECT model FROM (
SELECT model, price
FROM pc
UNION
SELECT model, price 
FROM laptop
UNION 
SELECT model, price 
FROM printer
)
WHERE price IN (
SELECT MAX(price)
FROM (
SELECT price
FROM pc
UNION
SELECT price 
FROM laptop
UNION 
SELECT price 
FROM printer
)
)

-- *25. Найдите производителей принтеров, которые производят ПК с наименьшим объемом RAM 
-- и с самым быстрым процессором среди всех ПК, 
-- имеющих наименьший объем RAM. Вывести: Maker

SELECT DISTINCT maker FROM product INNER JOIN printer
ON product.model = printer.model
WHERE maker IN (
  SELECT maker FROM product INNER JOIN pc 
  ON product.model = pc.model
  WHERE product.type = 'PC' AND pc.model IN (
    SELECT model FROM pc 
    WHERE speed = (
      SELECT MAX(speed) FROM pc
      WHERE ram = (
        SELECT MIN(ram) FROM pc
      )
    ) AND ram = (SELECT MIN(ram) FROM pc)
  )
)

-- 26. Найдите среднюю цену ПК и ПК-блокнотов, выпущенных производителем A (латинская буква). 
-- Вывести: одна общая средняя цена.

SELECT AVG(price) FROM (
  SELECT price, maker FROM product INNER JOIN pc
  ON product.model = pc.model
  WHERE product.type = 'PC'
  UNION ALL
  SELECT price, maker FROM product INNER JOIN laptop
  ON product.model = laptop.model
  WHERE product.type = 'Laptop' 
) WHERE maker = 'A'

-- *27. Найдите средний размер диска ПК каждого из тех производителей, которые выпускают и принтеры. 
-- Вывести: maker, средний размер HD.

SELECT maker, AVG(hd) FROM product INNER JOIN pc
ON product.model = pc.model
WHERE product.type = 'PC'
GROUP BY maker
HAVING maker IN (
  SELECT maker FROM product INNER JOIN printer
  ON product.model = printer.model
  WHERE product.type = 'Printer'
)

-- 28. Используя таблицу Product, определить количество производителей, выпускающих по одной модели.

SELECT COUNT(maker) AS QTY
FROM (
  SELECT maker FROM product
  GROUP BY maker
  HAVING count(model) = 1
)

-- 29. В предположении, что приход и расход денег на каждом пункте приема фиксируется не чаще одного раза в день [т.е. первичный ключ (пункт, дата)], 
-- написать запрос с выходными данными (пункт, дата, приход, расход). 
-- Использовать таблицы Income_o и Outcome_o.

SELECT DISTINCT point, date, 
 CASE 
 WHEN inc IS NULL 
 THEN '' 
 ELSE CAST(inc AS CHAR(20)) 
 END inc 
, 
CASE 
 WHEN out IS NULL 
 THEN '' 
 ELSE CAST(out AS CHAR(20)) 
 END 
FROM (
SELECT Income_o.point, Income_o.date, inc, out
FROM Income_o LEFT JOIN Outcome_o
ON Income_o.date = Outcome_o.date AND Income_o.point = Outcome_o.point
UNION
SELECT Outcome_o.point, Outcome_o.date, inc, out
FROM Income_o RIGHT JOIN Outcome_o
ON Income_o.date = Outcome_o.date AND Income_o.point = Outcome_o.point
) t1 ORDER BY point, date

-- *30. В предположении, что приход и расход денег на каждом пункте приема фиксируется произвольное число раз 
-- (первичным ключом в таблицах является столбец code), требуется получить таблицу, 
-- в которой каждому пункту за каждую дату выполнения операций будет соответствовать одна строка.
-- Вывод: point, date, суммарный расход пункта за день (out), суммарный приход пункта за день (inc). 
-- Отсутствующие значения считать неопределенными (NULL).

SELECT point, date, SUM(out), SUM(inc)
FROM (
SELECT income.point, income.date, out, inc
FROM income LEFT JOIN outcome 
ON Income.date = Outcome.date AND Income.point = Outcome.point
UNION
SELECT outcome.point, outcome.date, out, inc
FROM income RIGHT JOIN outcome 
ON Income.date = Outcome.date AND Income.point = Outcome.point
) t1 GROUP BY point, date
ORDER BY point, date
