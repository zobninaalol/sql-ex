-- 11. Найдите среднюю скорость ПК.

SELECT AVG(speed)
FROM pc

-- 12. Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.

SELECT AVG(speed)
FROM laptop
WHERE price > 1000

-- 13. Найдите среднюю скорость ПК, выпущенных производителем A.

SELECT AVG(speed)
FROM pc
WHERE model IN (
SELECT model
FROM product
WHERE maker = 'A'
)

-- 14. Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.

SELECT ships.class, name, country
FROM ships LEFT JOIN classes
ON ships.class = classes.class
WHERE numGuns >= 10

-- 15. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD

SELECT hd
FROM pc 
GROUP BY hd
HAVING COUNT(hd) >= 2

-- 16. Найдите пары моделей PC, имеющих одинаковые скорость и RAM. 
-- В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), 
-- Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.

SELECT DISTINCT a.model as model, b.model as model, a.speed, a.ram
FROM pc a, pc b
WHERE a.model > b.model AND a.speed = b.speed AND a.ram = b.ram

-- 17. Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
-- Вывести: type, model, speed

SELECT DISTINCT type, laptop.model, speed
FROM laptop INNER JOIN product
ON laptop.model = product.model
WHERE type = 'Laptop' AND speed < ALL(
SELECT speed
FROM pc
)

-- 18. Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price

SELECT DISTINCT maker, price
FROM printer INNER JOIN product
ON printer.model = product.model
WHERE product.type = 'Printer' AND color = 'y'
AND price = (
SELECT MIN(price) 
FROM printer INNER JOIN product
ON printer.model = product.model
WHERE product.type = 'Printer' AND color = 'y'
);

-- 19. Для каждого производителя, имеющего модели в таблице Laptop, 
-- найдите средний размер экрана выпускаемых им ПК-блокнотов.
-- Вывести: maker, средний размер экрана.

SELECT maker, AVG(screen) AS AVG_SCREEN
FROM product INNER JOIN laptop
ON product.model = laptop.model
WHERE type = 'Laptop' 
GROUP BY maker

-- 20. Найдите производителей, выпускающих по меньшей мере три различных модели ПК. 
-- Вывести: Maker, число моделей ПК.

SELECT maker, COUNT(product.model) AS count_model
FROM product 
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(product.model) >= 3
